require 'spec_helper'
require_relative '../app/models/here_conection'

describe 'HereConection' do
  let(:lat_o) { '-34.6060' }
  let(:long_o) { '-58.4570' }
  let(:lat_d) { '-34.6094484' }
  let(:long_d) { '-58.4558905' }
  let(:api_key) { 'lHy6DVRFGqv6Idw-SF_Rnkq6TdDdKpYUez5-fmVL-0Q' }

  it 'Request a here conection realizado correctamente' do
    WebMock.allow_net_connect!
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    response = HereConection.request(api_key, viaje.origen, viaje.destino, 'car')
    expect(response.status).to eq(200)
  end

  it 'HereConection duration devuelve la duracion del viaje en minutos' do
    WebMock.allow_net_connect!
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    response = HereConection.request(api_key, viaje.origen, viaje.destino, 'car')
    duration = HereConection.duration(response)
    expect(duration).to be >= 2
  end

  it 'No es posible calcular la duracion de un response fallido' do
    WebMock.allow_net_connect!
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    response = HereConection.request(api_key, viaje.origen, nil, 'car')
    expect { HereConection.duration(response) }.to raise_error(ArgumentError, 'Hubo un error llamando a la api de routs')
  end
end
