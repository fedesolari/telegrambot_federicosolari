require 'spec_helper'
require_relative '../app/models/viaje'

describe 'Viaje' do
  let(:lat_o) { '-34.6060' }
  let(:long_o) { '-58.4570' }
  let(:lat_d) { '-34.6094484' }
  let(:long_d) { '-58.4558905' }

  it 'Viaje con latitud -34.6060 se crea correctamente ' do
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    expect(viaje.latitud_origen).to eq(lat_o)
  end

  it 'Viaje con longitud 58.4570 se crea correctamente ' do
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    expect(viaje.longitud_origen).to eq(long_o)
  end

  it 'Viaje con latitud -34.6094484 se crea correctamente ' do
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    expect(viaje.latitud_destino).to eq(lat_d)
  end

  it 'Viaje con longitud -58.4558905 se crea correctamente ' do
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    expect(viaje.longitud_destino).to eq(long_d)
  end

  it 'Viaje origen devuelve la latitud y longitud del origen separado por una coma de url' do
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    origen = "#{lat_o}%2C#{long_o}"
    expect(viaje.origen).to eq(origen)
  end

  it 'Viaje destino devuelve la latitud y longitud del destino separado por una coma de url' do
    viaje = Viaje.new(lat_o, long_o, lat_d, long_d)
    destino = "#{lat_d}%2C#{long_d}"
    expect(viaje.destino).to eq(destino)
  end
end
