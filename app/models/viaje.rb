class Viaje
  attr_reader :latitud_origen, :longitud_origen, :latitud_destino, :longitud_destino

  def initialize(latitud1, longitud1, latitud2, longitud2)
    raise ArgumentError, 'El viaje debe tener una latitud y logitud de origen y destino' if latitud1.nil? || longitud1.nil? || latitud2.nil? || longitud2.nil?

    @latitud_origen = latitud1
    @longitud_origen = longitud1

    @latitud_destino = latitud2
    @longitud_destino = longitud2
  end

  def origen
    "#{@latitud_origen}%2C#{@longitud_origen}"
  end

  def destino
    "#{@latitud_destino}%2C#{@longitud_destino}"
  end
end
