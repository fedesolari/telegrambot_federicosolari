require "#{File.dirname(__FILE__)}/../../lib/routing"
class HereConection
  def self.request(api_key, origen, destino, _transport_mode)
    connection = Faraday::Connection.new 'https://router.hereapi.com'
    transport_mode = 'car'
    return_type = 'summary'
    routes_url = "/v8/routes?apiKey=#{api_key}&transportMode=#{transport_mode}&origin=#{origen}&destination=#{destino}&return=#{return_type}"
    connection.get routes_url
  end

  def self.duration(response)
    raise ArgumentError, 'Hubo un error llamando a la api de routs' unless response.success?

    json_response = JSON.parse(response.body)
    routes_list = json_response['routes']
    first_route = routes_list[0]
    first_section = first_route['sections'][0]
    duration = first_section['summary']['duration']
    duration / 60
  end
end
